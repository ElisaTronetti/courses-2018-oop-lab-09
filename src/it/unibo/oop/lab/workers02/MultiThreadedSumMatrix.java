package it.unibo.oop.lab.workers02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This is a standard implementation of the calculation.
 *
 */
public class MultiThreadedSumMatrix implements SumMatrix {

    private final int nThread;

    /**
     *
     * @param nThread number of thread that I want to create
     */
    //costruttore per sapere quanti thread userò questa volta
    public MultiThreadedSumMatrix(final int nThread) {
        super();
        if (nThread < 1) {
            throw new IllegalArgumentException();
        }
        this.nThread = nThread;
    }

    //creo dei workers che si suddivideranno il lavoro
    private static class Worker extends Thread {
        private final double[][] matrix;
        private final int startpos;
        private final int nelem;
        private double res;

        Worker(final double[][] matrix, final int startpos, final int nelem) {
            super();
            this.matrix = Arrays.copyOf(matrix, matrix.length);
            this.startpos = startpos;
            this.nelem = nelem;
        }
        @Override
        public void run() {
            for (int i = this.startpos; i < matrix.length && i < this.startpos + this.nelem; i++) {
                for (final double d : this.matrix[i]) {
                    res += d;
                }
            }
        }

        public double getResult() {
            return this.res;
        }
    }


    @Override
    public final double sum(final double[][] matrix) {
        final int size = matrix.length % this.nThread + matrix.length / this.nThread;
        final List<Worker> workers = new ArrayList<>(this.nThread);
        for (int start = 0; start < matrix.length; start += size) {
            workers.add(new Worker(matrix, start, size));
        }
        for (final Thread worker: workers) {
            worker.start();
        }

        double sum = 0;
        for (final Worker w: workers) {
            try {
                w.join();
                sum += w.getResult();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        /*
         * Return the sum
         */
        return sum;
    }

}
