
package it.unibo.oop.lab.reactivegui03;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * Counter.
 * After 10 seconds everything get stopped
 *
 */
public class AnotherConcurrentGUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton end = new JButton("Stop");
    private final JButton down = new JButton("Down");
    private final JButton up = new JButton("Up");
    private volatile boolean stop;

    /**
     * Constructor.
     */
    public AnotherConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        LayoutManager layout = new BorderLayout();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        //ho cambiato per conto mio il layout
        //perchè lo credo più chiaro
        //ma potenzialmente non cambia nulla
        final JPanel panel = new JPanel();
        final JPanel upperPanel = new JPanel();
        final JPanel centerPanel = new JPanel();
        panel.setLayout(layout);
        upperPanel.add(end);
        upperPanel.add(up);
        upperPanel.add(down);
        centerPanel.add(display);
        panel.add(upperPanel, BorderLayout.NORTH);
        panel.add(centerPanel, BorderLayout.CENTER);

        this.getContentPane().add(panel);
        this.setVisible(true);

        final Agent agent = new Agent();
        final Agent2 agent2 = new Agent2(); //inizia un thread che aspetta 10 secondi e poi modifica la variabile stop per fare fermare tutto
        new Thread(agent).start();
        new Thread(agent2).start();

        //aggiungo l'action listener di modo che quando clicco stop
        //si ferma il conteggio del tutto
        end.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent arg0) {
                agent.stopCounting();
            }

        });

        //setto il flag a true per fare crescere i numeri
        up.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent arg0) {
                agent.directionUp();
            }

        });

        //setto il flag a false per fare decrescere i numeri
        down.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent arg0) {
                agent.directionDown();
            }

        });
    }

    /**
     * @param stop I need to set the field stop
     */
    public void setStop(final boolean stop) {
        this.stop = stop;
    }

    /**
     * @return stop value
     */
    public boolean getStop() {
        return this.stop;
    }

    private class Agent implements Runnable {
        //stop e direction li metto volatile di modo di essere sicura che il dato che prendo sia effettivamente quello corrente

        private volatile boolean direction = true; //per farlo partire di modo che i numeri stiano crescendo
        private int counter;
        private static final int TIMER = 100;

        @Override
        public void run() {
            while (!getStop()) { //finché il flag stop è su false, il conteggio continua
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        @Override
                        public void run() {
                            AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    //cresco o decresco i numeri in base al flag
                    //potrei scrivere anche: direction ? counter++ : counter--;
                    if (direction) {
                        this.counter++;
                    } else {
                        this.counter--;
                    }
                    Thread.sleep(TIMER);
                } catch (InvocationTargetException | InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }

        public void stopCounting() {
            setStop(true);
        }

        public void directionUp() {
            this.direction = true; //fa crescere i numeri
        }

        public void directionDown() {
            this.direction = false; //fa decrescere i numeri
        }
    }


    private class Agent2 implements Runnable {
        private static final int TIMER = 10000;
        @Override
        public void run() {
            try {
                Thread.sleep(TIMER);
                setStop(true);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


