package it.unibo.oop.lab.workers01;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 *
 * TestMatrix for worker 1.
 *
 */
public class TestListSumClassic {

    /**
     * SumList and its multithreaded implementation are given as reference
     * implementation of a software that sums the elements of a list.
     *
     * Note that it is often impossible to split the load in an exact equal
     * manner - that's not an issue normally, however.
     */
    private static final int SIZE = 10000000;
    private static final String MSEC = " msec";

    /**
     * Base test for a multithreaded list sum.
     */
    @Test
    public void testBasic() {
        /*
         * Initialize a list
         */
        //creo una lista di 10 milioni di elementi
        final List<Integer> list = new ArrayList<>(SIZE);
        //creo la variabile sum che mi sommerà tutti gli elementi della lista
        long sum = 0;
        //creo un ciclo for che va da 0 a 10 milioni
        for (int i = 0; i < SIZE; i++) {
            //aggiungo i vari numeri nella lista
            list.add(i);
            //sommo i numeri precedenti con quello corrente
            sum += i;
        }
        //stampo qual è la somma
        System.out.println("BTW: the sum with " + SIZE + " elements is: " + sum);
        /*
         * Prepare time ant test with different number of threads
         */
        long time;
        //creo un array con vari numeri che mi indicheranno quando thread voglio usare (il foreach serve per usare ogni giro un numero diverso di thread)
        for (int threads:new int[] { 1, 2, 3, 8, 16, 32 }) {
            final SumList sumList = new MultiThreadedListSumClassic(threads); //questo costruttore prende in input il numero di thread (scorrendo l'array)
            time = System.currentTimeMillis(); //calcola il tempo
            assertEquals(sum, sumList.sum(list)); //vede se i due numeri sono uguale
            System.out.println("Tried with " + threads + " thread: "
                    + (System.currentTimeMillis() - time) + MSEC);
        }
    }

}
