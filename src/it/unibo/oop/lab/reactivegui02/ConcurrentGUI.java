package it.unibo.oop.lab.reactivegui02;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * Contatore di numeri.
 * Posso farli contare i verso crescente e decrescente in base ai bottoni che premo.
 *
 */
public class ConcurrentGUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("Stop");
    private final JButton down = new JButton("Down");
    private final JButton up = new JButton("Up");

    /**
     * Constructor.
     */
    public ConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        LayoutManager layout = new BorderLayout();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        //ho cambiato per conto mio il layout
        //perchè lo credo più chiaro
        //ma potenzialmente non cambia nulla
        final JPanel panel = new JPanel();
        final JPanel upperPanel = new JPanel();
        final JPanel centerPanel = new JPanel();
        panel.setLayout(layout);
        upperPanel.add(stop);
        upperPanel.add(up);
        upperPanel.add(down);
        centerPanel.add(display);
        panel.add(upperPanel, BorderLayout.NORTH);
        panel.add(centerPanel, BorderLayout.CENTER);

        this.getContentPane().add(panel);
        this.setVisible(true);

        final Agent agent = new Agent();
        new Thread(agent).start();

        //aggiungo l'action listener di modo che quando clicco stop
        //si ferma il conteggio del tutto
        stop.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent arg0) {
                agent.stopCounting();
            }

        });

        //setto il flag a true per fare crescere i numeri
        up.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent arg0) {
                agent.directionUp();
            }

        });

        //setto il flag a false per fare decrescere i numeri
        down.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent arg0) {
                agent.directionDown();
            }

        });
    }

    private class Agent implements Runnable {
        //stop e direction li metto volatile di modo di essere sicura che il dato che prendo sia effettivamente quello corrente
        private volatile boolean stop;
        private volatile boolean direction = true; //per farlo partire di modo che i numeri stiano crescendo
        private int counter;

        @Override
        public void run() {
            while (!this.stop) { //finché il flag stop è su false, il conteggio continua
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        @Override
                        public void run() {
                            ConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    //cresco o decresco i numeri in base al flag
                    //potrei scrivere anche: direction ? counter++ : counter--;
                    if (direction) {
                        this.counter++;
                    } else {
                        this.counter--;
                    }
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }

        public void stopCounting() {
            this.stop = true;
        }

        public void directionUp() {
            this.direction = true; //fa crescere i numeri
        }

        public void directionDown() {
            this.direction = false; //fa decrescere i numeri
        }
    }
}
